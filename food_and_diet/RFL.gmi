Rapid Fat Loss

# High Protein, Low Carb, Low Fat

## Protein Sources

### Proteinpowders
IF they are almost only protein: ok

### Lupin Flour?
OK, mostly protein & fiber
Pretoasted, skins removed so less fiber than full bean but easier to digest

### Seitan?
Should be ok, combine with lupin

### Soy?
Too much oil?

### Cashew?
Probably too much fat

### Pepitas?

### Hempseed?

### Flax/Chia?

### Grains? Quinoa? Amaranth?
You're funny, unless you can make seitan out of it.

### Dairy?
Milk is too high in fat and carbs
Fatfree cheese
EggsWhites

### Cowpeas, leafy tips?
aka black eyed peas
this was under "vegetables"
what's up with the leafy tips? some kind of sprout?

### Other beans?
Too Much Starch 60% vs 20% protein
Some score well in sprouted form:
kidney beans, sprouted: 29gr of protein/200 Kcal


## Some carb is needed
we are talking 5-10g per day
combined in pure protein shake (not lupin flour)
Some tissue types cannot operate on ketones
they need to perform gluconeogensis, which can be from protein
not sure the nutrition values for "sprouted" versions of food are reliable, because you don't know how much water was in them at the time of measuring


## Only omega 3 fat, and not much

## Fiber is needed, so eat vegetables
### Non starchy vegetables

## Supplements
### Try to get All Vitamins and Minerals from supplements
* avoids extra calories
* 

# Categories
## BMI to BF% 
### Charts in appendix1
1.83m @ 118kg = 35%
BW * bf% = LBM
118kg * (1 - 0.35BF)  = 76.7 kg LBM

## Bodyfat
category	male bf%	female bf%
1		<= 15%		<= 24%	
2		16-25%		25-34 %
3		>= 26%		>= 35%

# Eat only essential nutrients = those  your body cannot make itself
## Protein, see above
## daily protein intake
bfcategory	inactive	aerobics	weight training (and aerobics)
1		1-1.25		1.25-1.5	1.5-2.0
2		.9		1.1		1.25
3		.8		.9		1.0

It seems higher BF category needs less gr protein per kg lean mass:

LBM 76.7kg = 169 lbs * 0.8 = 135g = 540 kc
		     * 0.9 = 152g = 604 kc
		     * 1.0 = 169g = 676 kc




## Omega 3
some is needed, and avoids rabbit starvation (=eating only protein without any fat)


# Exercise
## Limited resistance training, 2-3x /week
### higher rep, lower weight 12-15
helps deplete muscle glycogen, burning more fat
## Minimal cardio
## walking is fine
## 20mins, 3x week for beginners
## 40mins, 3-5x week later on

### target all muscles
