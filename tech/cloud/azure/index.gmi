Azure notes: 
https://learn.microsoft.com/en-us/training/modules/azure-architecture-fundamentals/introduction

# azure-architecture-fundamentals

- Azure regions, region pairs, and availability zones.
- Azure resources, resource groups, and Azure Resource Manager.
- Azure subscriptions and management groups.

Management Group < Subscription < Resource Groups < Resources 

- Resources: 
  - Resources are instances of services that you create, like virtual machines, storage, or SQL databases.  
  - Need to be in an RG

- Resource groups: ~ no aws equivalent
    - Resources are combined into resource groups. Resource groups act as a logical container into which Related Azure resources like web apps, databases, and storage accounts are deployed and managed. 
    - Resource groups can't be nested
    - Can delete all resources at once
    - Scope for applying RBAC permissions
    



- Subscriptions: A subscription groups together user accounts and the resources that have been created by those user accounts. For each subscription, there are limits or quotas on the amount of resources that you can create and use. Organizations can use subscriptions to manage costs and the resources that are created by users, teams, or projects. ~ aws account

- Management groups: These groups help you manage access, policy, and compliance for multiple subscriptions. All subscriptions in a management group automatically inherit the conditions applied to the management group. ~ aws organizations

# Azure regions, region pairs, and availability zones.
## Region
- geographical location, low latency between resources
- required by most resources
- some regions do not have all resource types or sizes
- Global services:
  - Azure AD
  - Azure Traffic Manager
  - Azure DNS
- Azure has more global regions than any other cloud provider.
- Special regions
  - US govt: dod, state govts
  - China: 21Vianet, no MS datacenters
## Availability zones
- within 1 region
- connected through high-speed, private fiber-optic networks.
- each az = 1 or more physical datacenters
- Not every region has support for availability zones. [list here](https://learn.microsoft.com/en-us/azure/availability-zones/az-region)

Availability zones are primarily for VMs, managed disks, load balancers, and SQL databases. The following categories of Azure services support availability zones:

- Zonal services: You pin the resource to a specific zone (for example, VMs, managed disks, IP addresses).
- Zone-redundant services: The platform replicates automatically across zones (for example, zone-redundant storage, SQL Database).
- Non-regional services: Services are always available from Azure geographies and are resilient to zone-wide outages and region-wide outages.

## **Region Pairs**
- Each Azure region is always paired with another region within the same geography (such as US, Europe, or Asia) at least 300 miles away.
  - examples:
    - West US paired with East US
    - SouthEast Asia paired with East Asia
- Directly connected and far enough apart for dr
- Some services offer automatic geo-redundant storage by using region pairs.
- More advantages of region pairs:

    - If an extensive Azure outage occurs, one region out of every pair is prioritized to make sure at least one is restored as quickly as possible for applications hosted in that region pair.
    - Planned Azure updates are rolled out to paired regions one region at a time to minimize downtime and risk of application outage.
    - Data continues to reside within the same geography as its pair (except for Brazil South) for tax- and law-enforcement jurisdiction purposes.


## Azure resources and Azure Resource Manager
- Azure Resource Manager provides a management layer that enables you to create, update, and delete resources in your Azure account
- management features like access control, locks, and tags to secure and organize your resources after deployment.
- "endpoint" for all interfaces: portal, SDK (powershell, cli), REST clients pass via Azure Resource Manager
- Manage your infrastructure through **declarative templates** rather than scripts. A Resource Manager template is a **JSON file** that defines what you want to deploy to Azure.
- like terraform, cloudformation: CRUD/redeploy whole series of resources at once
- Define the dependencies for deploy order.
- Apply access control to all services because RBAC is natively integrated into the management platform.
- Apply tags to resources to logically organize all the resources in your subscription.
- Clarify your organization's billing by viewing costs for a group of resources that share the same tag.

## Azure subscriptions and management groups
### Azure subscriptions
- = logical unit/continainer of Azure services/resources that **links to an Azure account**
- Are Required: authenticated and authorized access to Azure products and services, provision resources
- logical unit of Azure services that links to an Azure account = identity in Azure Active Directory (Azure AD) or in a directory that Azure AD trusts.
- one or more per Azure account
- define Boundaries around Azure products, services, and resources. There are two types: 
  - **Billing boundary** y: This subscription type determines how an Azure account is billed for using Azure. You can create multiple subscriptions for different types of billing requirements. Azure generates separate billing reports and invoices for each subscription so that you can organize and manage costs.
  - **Access control boundary** : Azure applies access-management policies at the subscription level, and you can create separate subscriptions to reflect different organizational structures. An example is that within a business, you have different departments to which you apply distinct Azure subscription policies. This billing model allows you to manage and control access to the resources that users create within specific subscriptions.

- Reasons for additional subscription :
  - environments: dev / test/ uat / prod or to isolate data for compliance (! resource access control is at subscription level)
  - organizational structures:
    - lower certain teams to lower-cost resources ~ SCP in AWS?
  - Billing:
    - costs are first aggregated at subscription level
    - manage and track cost based on needs
    - separate billing for prod/nonprod
  - Subscription limits: Subscriptions are bound to some hard limitations. 
    For example, the maximum number of Azure ExpressRoute circuits per subscription is 10. Those limits should be considered as you create subscriptions on your account. If there's a need to go over those limits in particular scenarios, you might need additional subscriptions.

  - Organize multiple subscriptions into invoice sections
  - also: billing profiles within same billing account
    Billing account < Billing profiles < Invoice sections < subscriptions

### Azure management groups: give you enterprise-grade management at a large scale
- Groups (many) subscriptions
- efficiently manage access, policies, and compliance for subscriptions
 All subscriptions within a single management group must trust the same Azure AD tenant.
- create a hierarchy that applies a policy (~ aws SCP)
  eg "limit VM locations to the US West Region in a group called Production. "
- use management groups is to provide user access to multiple subscriptions
  - one role-based access control (RBAC) assignment on the management group, which will inherit that access to all the subscriptions
  - One assignment on the management group can enable users to have access to everything they need instead of scripting RBAC over different subscriptions.

nesting possible: Root Mgmt Group < mg1 < submg1, submg2,... < subscriptions

**Important facts about management groups**
- 10,000 management groups can be supported in a single directory.
- A management group tree can support up to six levels of depth. This limit doesn't include the root level or the subscription level.
- Each management group and subscription can support only one parent.
- Each management group can have many children.
- All subscriptions and management groups are within a single hierarchy in each directory.
