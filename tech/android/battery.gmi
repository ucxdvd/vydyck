Various tips to reduce battery and data usage

# Background behaviour
## Standby apps
In certain Android versions (mine is 12) you can tweak standby behaviour of apps.
Most apps come with an "App standby state" of "RARE".  In developer options, you can change this per app to:
### ACTIVE:
### WORKING_SET: 
### FREQUENT:
### RARE:

I wonder what the impact is of this...
