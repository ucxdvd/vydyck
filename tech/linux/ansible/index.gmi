# Ansible
docs.ansible.com
## Installation
* os packages tend to be out of date, but might be needed for (paid) support eg on RedHat
* pip allows installation of multiple ansible versions via python venv
* clone from github, see the "hacking" subdir.  Handle all prereqs yourself!
## inventories
### files
various types exist, default = ini style but yaml is also possible
### Dynamic inventories via inventory plugins

Many exist:
- ec2
- vmware
- foreman/satellite
- cobbler
... 
### Classic custom dynamic inventory
- inventory is not a file but an executable (chmox +x) in any language, can be a binary
- executable must output (stdout) a specific json structure
  - errors (stderr) are displayed as ansible errors
- inventory generator can happen remotely, the executable could just be a script that calls the remote inventory
  - eg "curl -L https://webserver/inventory.py
  - useful if inventory is generated from sensitive components you don't want to expose to the rest of the network
- no caching by ansible afaik; handle this in the executable itself
  - eg generate a timestamped json file, output it to stdout, and only recreate the json if it's more than an hour old
- PROS: when done well, this method is very flexible; basically anything can be an inventory source for your script
  - asset db's without plugins
  - you can base groups on ANYTHING:
    - in case of a DB query, create dynamic groups based on column values, and create special code for particular columns
    - exclude non-ansible managable hosts,... 
- a lot of work to get exactly right (invalid character names, missing attributes,... etc), dealing with data quality issues (typos in db, conversion to lowercase, ...)
       
### Modern dynamic inventory: write your own plugin
See ansible docs, these have to be in python

## playbooks
## roles
## Collections
### Creating and updating
* ansible-galaxy collection init my_namespace.my_collection
** update my_namespace/my_collection/galaxy.yml between builds to update the version
* ansible-galaxy collection build my_namespace/my_collection
* ansible-galaxy collection publish 
* ansible-galaxy collection install
### Layout
=> collection-structure.gmi
=> https://docs.ansible.com/ansible/latest/dev_guide/developing_collections_structure.html#collection-structure official structure

