# Change keyboard on linux console
* loadkeys <countrycode> no longer seems to work
* instead update /etc/default/keyboard [debian]
* you may need to rerun setupcon after that
# Default editor (Debian, ubuntu)
sudo update-alternatives --config editor
