msi-gs73-stealth-8rf-020nl

# pros & cons
## pros
- 4k screen extremely nice, 120hz (only got this in linux...)
- powerful i7 cpu
- "ready for win7"
- nvidia gtx 1070 maxq : now considered "midrange" and no raytracing support
- nice build: aluminum, steelseries keyboard
- 3 usb3 ports, 1 usbc
- thunderbolt3 ( on usbc or minidp - not sure) means gpu could be externalized reducing heat, and allowing upgrades
- very good speakers

## cons
- storage limited: 0.5tb ssd(nvme) 1tb mechanical (sata 2.5in)
- limited linux support: so far only manjaro & kali worked
- weak batterylife and cannot operate without battery.  battery wore out badly over only 4y (probably about 3y semiregular use)
- becomes very hot
- very noise fans to keep it from overheating
- design flaws: hinge prone to breaking, and now too loose.  wifi drops out after ~30mins.  Series of dead pixels in middle of very expensive screen.
- designed to be hard to repair and upgrade, opening voids warranty (expired now anyway)


Situation
---------
- batterylife was alwasy pathetic, and performance when not plugged in was very bad (games not possible) : almost pointless as a laptop?
- hinge broke once, and now lid is so loose it can fall forward or backward under its own weight -> "FIXED"
- seemed to run hotter than I remember: fans wearing out? never cleaned them, since cannot open it...
- very visible and ugly permanently red dots (dead/stuck pixels) in middle? never saw something like that before...
- several steelseries keys stay permanently red when on: keyboard may be dying
- wifi became unreliable and drops out after 30-60min
- sometimes stuck at poweroff: machine hangs permanently, and longpress on power would not turn it off.  "Thankfully" powered down after a few minutes because batterylife was so short at the end.
- finally, after one such event, battery did not charge and machine did not turn on even after being plugged in for days

Pay for repairs?
----------------
- if mobo is broken, could be > 800euro
- battery & fan replacement > 450 euro alledgedly
- may need to replace wifi too
- might finally replace silly mechanical hd with 3tb ssd, and bigger nvme.
=> will be a long cycle of repairs, from 1 broken part to another, for a laptop that's not readly exceptional in specs anymore.
=> what about my data on the hd & nvme right now...
	- idea: open it up, take these out and erase them...?
		- at least wipe all data

Repair myself?
--------------
- will need to keep buying and replacing parts until it works again
- at least battery and other power-related parts
- replace wifi card?
- good get expensive and frustrating, but otherwise "interesting" learning opportunity
=> tricky with 3rd party replacements, recommended to only use "oem" parts
	- msi is being difficult, no parts store
=> ... or, a good lesson on not buying such an unrepairable device again (even by laptop standards)


Find similar refurbished laptop, exchange parts?
------------------------------------------------
- 350 euro gs73 *8re* seems like a good deal, and already had repairs
	- why is he ditching it? he already dropped it price twice.
- partially broken keyboard
- what else is about to break?
- 8re was lower spec (gpu, battery, camera, storage, ...) so would have to transplant lots of parts; might as well buy the parts
=> can't I find a corei7+mobo for 350e?
	if ddr3 I can reuse clarkdale's memory
=> may end up with 2 broken machines
=> for 350e I can build a ryzen5 system with twice the ram, and much more flexible storage

What to do?
-----------
- Do I need a gaming laptop? that needs a powercord all the time? and is hot and noisy? NO
	- gamestream from gaming pci to any laptop/tablet 
- Other uses of this machine?
	- 4k is nice to work on
	- recent corei7 is useful in nongpu stuff
	- usb3 is useful
	-> I can do all that with a small upgrade on the current gaming pc
- find if it's just the battery, and get a new one?
	- need to learn how to debug this
		- learn to use a multimeter (have one...)

Recycle what is reusable?
-------------------------
- storage, but not worth much (probably less than 100e worth): nvme to sata card 17-30 eur, just for 500gb. 1tb 2.5inch hd not that useful.
	- would not buy either of these new...
- convert deadpixel screen into standalone one with hdmi adapter?
	- 30-50 eur and quite a bit of work
	- might be worth it, but ugly red dots...
	- a new 4k screen of proper size starts at 200
- keyboard? how to convert to usb?
	- never cared for the stupid leds anyway, hard to get right per game and could not find many templates
	- don't even want it?
- wifi is pci-e mini, but might be damaged
- is gpu built into motherboard? but how to reuse pci-e?
- mobo? cpu?
	- frankly, my ancient corei5 (clarkdale) with nv gtx 1660super almost equals it in most games.
	- maybe just get a core i7&mobo with usb3 and move my 1660 in that?
- 16gb ddr4 seems good, but they're sodimm...
	- can this work on regular mobo?
	- sodimm to dimm adapters exist!
	- 2x8gb ddr4 = about 50eur

Specs
-----
MSI GS73 Stealth 8RF-011 (GS73 Series)
ProcessorIntel Core i7-8750H 6 x 2.2 - 4.1 GHz, Coffee Lake-H
Graphics adapterNVIDIA GeForce GTX 1070 Max-Q - 8 GB VRAM, Core: 1101 MHz, Memory: 8000 MHz, GDDR5, ForceWare 390.94, Optimus

Memory16 GB  , 2x 8 GB SO-DIMM DDR4-2400, dual channel, all slots occupied, max. 32 GB
Max 32GB, DDR4-2400
DDR4-2666, 2 Slots,

Display 17.3" UHD (3840x2160), IPS-Level, 120 Hz, 4k
MainboardIntel HM370

StorageSamsung PM961 MZVLW256HEHP, 256 GB  
, NVMe SSD + Seagate BarraCuda ST2000LM015, 2 TB HDD, 5400 rpm
-> does not seem to match my specs...
1x M.2 SSD Combo slot (NVMe PCIe Gen3 / SATA)
1x M.2 SSD Combo (NVMe PCIe Gen3 / SATA )
1x 2.5" SATA HDD

Soundcard Realtek ALC1220 @ Intel Cannon Lake PCH
Connections
2x Type-A USB3.1 Gen1
1x Type-A USB3.1 Gen2
1x Type-A USB2.0
1x SD (XC/HC) Card Reader
1x (4K @ 60Hz) HDMI™
1x Mini-DisplayPort
1x RJ45

BATTERY	3-Cell 51

Audio Connections: 1x Mic-in 1x Headphone-out (HiFi / SPDIF)
Card Reader: SD, SDHC, SDXC
Networking Killer E2500 Gigabit Ethernet Controller (10/100/1000MBit/s), Killer Wireless-AC 1550 Wireless Network Adapter (a/b/g/h/n = Wi-Fi 4/ac = Wi-Fi 5), Bluetooth 5.0
Sizeheight x width x depth (in mm): 19.6 x 412 x 285 ( = 0.77 x 16.22 x 11.22 in)
Battery51 Wh, 4500 mAh Lithium-Polymer, 3 cells
Operating SystemMicrosoft Windows 10 Home 64 Bit
CameraWebcam: HD (720p) 30fps
Additional features
Speakers: 4x 2W speaker, 1x 3W subwoofer, 
Keyboard: Chiclet RGB, Keyboard Light: yes, "Steel series"

180-Watt power supply, Quick Start guide, warranty booklet, recovery instructions, various manufacturer tools, Killer Performance Suite, Norton Security trial, XSplit Gamecaster trial, 24 Months Warranty
Weight2.41 kg ( = 85.01 oz / 5.31 pounds), Power Supply: 420 g ( = 14.82 oz / 0.93 pounds)


Known parts
-----------
amazon seems to have gs73 parts (at least, they support returns...)
- power supply/charger: => https://www.amazon.de/-/en/sspa/click?ie=UTF8&spc=MTo2MjkwMjgyOTc4OTQ3NzA3OjE2NzQ5NTY1NzA6c3BfYXRmOjIwMDY4MzIwNzk2MjA3OjowOjo&url=%2FCharger-Creator-MS-16V1-ADP-180TB-A17-180P4A%2Fdp%2FB092ZHYZ8J%2Fref%3Dsr_1_1_sspa%3Fcrid%3D3MWLPUZAJ4WTC%26keywords%3Dmsi%2Bgs73%26qid%3D1674956570%26sprefix%3Dmsi%2Bgs73%252Caps%252C113%26sr%3D8-1-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9hdGY%26psc%3D1
- battery:
=> https://www.sparepartworld.com/msi/notebook/gs-serie/gs73-stealth-8rf-ms-17b7/battery-58391364 #ouch, 170$$
=> https://www.amazon.com/7XINbox-64-98Wh-5700mAh-BTY-M6J-Replacement/dp/B07C2TJ7P3

