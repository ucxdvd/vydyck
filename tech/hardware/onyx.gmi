september? 1996 - Edwin Waelbers aka "onyx computers"
	Specs:
	pentium1, 133mhz
	32 mb ram
	pci mobo, something fancy?
	s3 trio64 v+ videocard

	recuperated hardware from 486 (q1)
		tapedrive (kept until the end)
		soundcard (also kept until the end)
		harddrives (expanded later)
		modem upgraded
		cdrom drive upgraded twice or even 3x
		reuse of q1 minitowercase replaced almost immediately

Assembly worked, but made an error on the mobo powercable (should have been "black against black")
	
Revision 1, same week
		- full tower AT(?), new powersupply
			makes life easier with all those devices we needed back then
	I paied Edwin to overlook my assembly in his shop, and got some tips from him.
Revision 2, also same week
		- 1.6 gb harddrive, now about 2.2gb in total

So many upgrades.. an addiction?


Upgrade ??: 33.6k modem

Upgrade ??: 16x speed cdrom drive, ide for cdrom boot support

Upgrade ??: 24x speed cdrom drive, ide
	after 16x speed drive became unreliable

Upgrade ??: 4.x gb Quantoom "Bigfoot" harddrive

Upgrade ??: 1997 or 8? CDRW writer, SCSI
	Ricoh mp6200
		Unstable, lots of bad discs
		Fixed by moving it into a separate scsi case
	Adaptec isa crappy scsi controller (1515? 1520?)
		no boot support

Upgrade??: scsi small tower (2x5.25) case
	my first external storage device, stored ricoh cdrw rewriter
	later stored 4mm scsi tapedrive (Thank you stork)

Upgrade ??: Buslogic ?? uw scsi controller

Upgrade ??: 9gb uw scsi hd

Upgrade ??: 18.4gb uw scsi hd

Upgrade ??: matrox millenium 2

Upgrade ??: 3dfx voodoo1, first real gpu

Upgrade ??: isa ethernet adapters, replacing the silly serial and parallell (worked only in linux) solutions

Upgrade ??: 1997 17inch sony trinitron monitor. 

Upgrade ??: removable hd trays 

Somehow, never did a memory upgrade, I think.

Osses used:
	dos 6.22? + wind3.11, still
	os2 warp
	linux slackware, redhat, suse from infomagic cdsets
	win95, 98x later, nt4, maybe even win2k?

System never broke, but became unstable when starting with scsi cdrw.  Extra cooling fans and externalizing the cdrw stuff helped, and switching to a proper scsi controller.  Main system for about 4y, replaced in 2000 with an athlon system, also full tower.  Kept as a server for much longer than that.

