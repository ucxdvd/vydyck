# Easy Vegan Mayo

=> https://simple-veganista.com/vegan-mayo/print/17804/  Original

* Author: Julie | The Simple Veganista
* Prep Time: 2 minutes
* Total Time: 2 minutes
* Yield: Makes about 1 1/4 cups 1 x
* Category: Condiment
* Method: mix
* Cuisine: American
* Diet: Vegan


### Ingredients

Units US 

* 7 oz. organic tofu (silken or soft)
* 1/3 cup (79ml) neutral oil (I used light flavored olive oil)
* 1 tablespoon lemon juice (about 1/2 medium lemon)
* 1 teaspoon apple cider vinegar
* 1/4 teaspoon salt, + more to taste
* 1/4 teaspoon dijon mustard

### Instructions

Small Personal Blender: Simply add everything to the cup, attach the criss-cross blade, and blend for 1 minute. Taste for flavor. This is my preferred method and love my NutriBullet (affiliate link ) for small jobs like this!

Immersion Blender: Place the ingredients into a 2-cup measuring cup or tall straight glass (a mason jar would be great), and using the immersion blender, blend for 30 seconds to 1 minute, until creamy. Taste for flavor.

Regular Blender: This will work best when doubling this recipe. Simply place the ingredients in the blender, and process until smooth and creamy, about 1 minute, stopping to scrape down the sides as needed. Taste for flavor.


Store in an airtight container in the refrigerator for up to 1 week.

### Notes

If you don’t care for dijon, try adding 1/4 teaspoon onion powder.

If you don’t have both lemon and vinegar on hand, taste for flavor with the one ingredient you do have, adding a little more as needed, a teaspoon at a time.

To limit the oil, use 1/4 cup. Alternately, add more, if you prefer, up to 1/2 cup.

Add more salt as needed. The mayo should not be bland tasting, and if so, it can use more salt.

# Alternate recipes
=> https://theplantbasedschool.com/vegan-mayo/#ingredients-substitutions soymilk instead of tofu
=> https://veeg.co/vegan-sunflower-seed-mayo/ sunflower seed instead of soymilk,tofu or oil
=> vegan-sunflower-seed-mayo local copy
=> https://www.pennilessparenting.com/2013/03/homemade-vegan-mayonnaise-recipe-flax.html from flax seeds and oil
=> veganaise-flax.gmi local copy
The trick? Well, there are a few. First, make sure your flax seeds are very finely ground for better emulsion and texture. Second, heat the flax and water to form a thick paste. Third, add a small amount of oil at a time, then mix and repeat. The method detailed in the link below worked much better for us than either drizzling the oil in a slow stream or adding it all at once. We had the most success with this recipe from Penniless Parenting
=> https://recipes.sparkpeople.com/recipe-detail.asp?recipe=2603328 one more with flax
