# Food related stuff
## Spices
=> spices/index.gmi
## Sprouting
=> sprouting/index.gmi
## Fermentation
=> fermentation/index.gmi
## Preservation
=> preservation/index.gmi
## tips & tricks
=> tipsandtricks/index.gmi
## Recipes
=> recipes/index.gmi
