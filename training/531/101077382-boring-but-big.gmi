=> https://www.jimwendler.com  
=> https://www.jimwendler.com/blogs/jimwendler-com/101077382-boring-but-big

# Boring But Big

## 

Without a doubt, the Boring But Big is the most popular assistance template for the 5/3/1 training program. This is because Boring But Big is easy to program, easy to use and great for gaining both strength and size.  There are two basic ways to do the Boring But Big template:

=> https://www.jimwendler.com/collections/books-programs/products/5-3-1-for-powerlifting-ebook-version   5/3/1 training program.

### Boring But Big Example 1
Day One

* Press – 5/3/1
* Press – 5 sets of 10 reps
* Lat work – 5 sets of 10 reps

Day Two

* Deadlift – 5/3/1
* Deadlift – 5 sets of 10 reps
* Abs – 5 sets

Day Three

* Bench Press – 5/3/1
* Bench Press – 5 sets of 10 reps
* Lat work – 5 sets of 10 reps

Day Four

* Squat – 5/3/1
* Squat – 5 sets of 10 reps
* Abs – 5 sets

### Boring But Big Example 2
Day One

* Press – 5/3/1
* Bench Press – 5 sets of 10 reps
* Lat work – 5 sets of 10 reps

Day Two

* Deadlift – 5/3/1
* Squat – 5 sets of 10 reps
* Abs – 5 sets

Day Three

* Bench Press – 5/3/1
* Press – 5 sets of 10 reps
* Lat work – 5 sets of 10 reps

Day Four

* Squat – 5/3/1
* Deadlift – 5 sets of 10 reps
* Abs – 5 sets

I get asked all the time which one is better.  There is no “better” just different.  And really, it’s the same amount of work, no matter which one you choose.  Some people like the second option because you are doing the lifts two times per week.  No matter which one you choose, you can also substitute the “5x10” exercise with a similar exercise.

### 

### Deadlift Variations

* Trap Bar Deadlift
* Snatch Grip Deadlift
* Deficit Deadlift
* Block Deadlift
* Rack Deadlift
* Straight Leg Deadlift

### Squat Variations

* Front Squat
* Box Squat
* Safety Bar Squat
* Leg Press
* Hack Squat

### Press/Bench Press Variations

* Incline Press
* Dumbbell Bench Press
* Dumbbell Incline Press
* Dumbbell Press
* Swiss Bar (Football Bar) Bench Press
* Swiss Bar (Football Bar) Incline Press
* Swiss Bar (Football Bar) Press
* Close Grip Bench Press
* Floor Press

For people new to the Boring But Big (BBB) program, I highly, highly recommend starting very light on the lower body assistance work.  Even if the weight seems very light and the sets easy, you will be insanely sore from squatting and deadlifting for 5 sets of 10 reps. There is no point in making yourself so sore that you cannot train the next day.  Or even two days after that.  So if you are new to the BBB, start with a light weight (around 30%) of your Training Max. Or another way of saying it; start with the lightest weight on the bar that won’t embarrass you.  Something that still keeps your ego from being completely and totally broken. The general rule of thumb is to use 50-60% of your Training Max for the 5 sets of 10 reps. But this is not set in stone; the whole goal is to get 5 sets of 10 reps. You can choose to do all the sets at one weight or vary the weight on the sets.

=> https://jimwendler.com/blogs/jimwendler-com/101082310-the-training-max-what-you-need-to-know?_pos=1&_sid=8e1b74bee&_ss=r  Training Max.

### 

### Ascending

* Set 1 – 30%
* Set 2 – 40%
* Set 3 – 50%
* Set 4 – 60%
* Set 5 – 70%

### Descending

* Set 1 – 70%
* Set 2 – 60%
* Set 3 – 50%
* Set 4 – 40%
* Set 5 – 30%

### Up/Down

* Set 1 – 50%
* Set 2 – 60%
* Set 3 – 70%
* Set 4 – 60%
* Set 5 – 50%

### NOTE:
The above are examples and any percentages can be used.  If you are using dumbbells for the lifts, just use your best judgment.  Don’t try to program dumbbell work.  Please.  Just pick heavier or lighter dumbbells. If you are using a different lift than the main lift (for example, you choose trap bar deadlifts instead of straight bar deadlifts) and want to program the percentages, you have two options: * Find your training max for that lift.
* Approximate your training max for that lift, erring on the side of “too light” for the first training cycle.
Don’t try to base your front squat max off of your squat max – or try to use some silly “this lift is usually about X% of that lift” non-sense that floats around the internet like syphilis.  If you are going to do something, do it right.  Another option that people have thrived on is to pick three weights for the 5x10 lift – heavy, medium and light. These don’t have to be percentages of the training max; rather three weights you choose.  For example, I like to use 225, 275 and 315.  225 is very easy, 275 is tougher but doable and 315 makes me feel like shit.  Do one cycle with the easy weight, the next cycle with the medium weight and the 3 rd cycle with the heaviest.  Repeat. For people completely new to this, I recommend this:

* Cycle One: 5 sets of 10 @ 30% of Training Max
* Cycle Two: 5 sets of 10 @ 45% of Training Max
* Cycle Three: 5 sets of 10 @ 60% of Training Max

Once you reach cycle three, you can use 60% of your Training Max for all cycles.  Because your Training Max goes up every cycle, so will the weights for your 5x10 work.  It self regulates with the 5/3/1 program.

=> https://www.jimwendler.com/collections/books-programs/products/5-3-1-for-powerlifting-ebook-version  5/3/1 program

### 

### Additional Notes for Boring But Big (BBB)

* For the deload, I recommend doing the 5x10 work or cutting it down to 3x10.  Use your best judgment and what you feel is best for your body.

* Whatever variations, percentages or exercises you choose, the important thing is to perform the 5x10 work.  It’s not so much about the weights used rather it is the work being done.  Don’t get caught up in a 5x10 Dick Measuring contest.

* For the 5x10 work and the deadlift, feel free to use straps.  If your grip is lacking, choose a weight that allows you to perform the sets with a double overhand grip.  Obviously, this is going to be much lighter than the 60% but it is a great way to get grip work into your training program.

* The Boring But Big program is best used in a 3 or 4 day training program.

* For many people, the 5x10 deadlift work is much too taxing.  If this is the case, substitute light good mornings or straight leg deadlifts for 5x10; this will keep your lower back and hamstring stimulated but not drained.

* You can superset the 5x10 pressing work with the lat work.  This will cut down on workout time and keep you moving.

* I recommend taking the belt off for the 5x10 squat and deadlift training. If you are not used to squatting or pulling without a belt, start lighter and work yourself up.

* You can substitute curls or shrugs for the lat work.

* Do not try to add more to this program – this is the biggest mistake people make with the BBB.  They think they need to do more.  If you feel you fall into this category then you are clearly not working hard enough on the 5/3/1 sets and/or the 5x10 work.

* You can push the final 5/3/1 sets – do the 5/3/1 program as you normally would.

=> https://www.jimwendler.com/collections/books-programs/products/5-3-1-for-powerlifting-ebook-version  5/3/1 program

* Your conditioning work during the Boring But Big program should reflect your goals and your training level.

* For the squat variations, you can try doing one-leg exercises (lunges, step ups, one-leg squats).  If you do this, I would limit the reps and perform 5 sets of 6 reps instead of 5 sets of 10 reps. Do not base the weights on any percentage.  You would just choose a weight and get the job done.

If you don't understand some, most or any of the above training or terminology, I highly recommend you read the 5/3/1 Second Edition which is available in both paperback and eBook.  It covers all the ground work and provides you with tons of programming options from which to choose, for many years to come.  Time is going to pass and nothing stays the same; you're either getting better or getting worse.


Antonin B.

That's why I like 531BoringButBig. With the structure being :
Heavy DL + light squat
Heavy OHP + light bench
Heavy Squat + light DL
Heavy Bench + light OHP
As base split (+some accessory), it gives alot of exposure to technique and different volume/intensity ranges.
