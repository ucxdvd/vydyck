* GET STARTED
* STORE

* 
* 
* 
* 
* 

[‡ starting strength wordmark fallback]

* 

=> https://startingstrength.com/get-started  GET STARTED
=> https://aasgaardco.com  STORE
=> https://www.facebook.com/pages/Starting-Strength-The-Aasgaard-Company/142424022490628  
=> https://twitter.com/SS_strength  
=> https://www.instagram.com/startingstrength  
=> https://www.youtube.com/user/AasgaardCo  
=> https://startingstrength.com/search  
=> https://startingstrength.com  

* What is Starting Strength?
* Get Started
* Articles
* Videos
* Podcasts
* Training Log
* Forums
* Gyms
* Equipment
* Coaching
* Events
* Store
* Careers
* About

* 
* 
* 
* 
* 

* Content Library

* Articles
* Videos
* Podcasts
* Training Log
* Report

* Podcast
* Forums
* Gyms
* Equipment
* Coaching

=> https://startingstrength.com/about  What is Starting Strength?
=> https://startingstrength.com/get-started  Get Started
=> https://startingstrength.com/article  Articles
=> https://startingstrength.com/video  Videos
=> https://startingstrength.com/radio  Podcasts
=> https://startingstrength.com/training  Training Log
=> https://startingstrength.com/resources/forum/  Forums
=> https://startingstrength.com/gyms/  Gyms
=> https://startingstrength.com/equipment  Equipment
=> https://startingstrength.com/coaching  Coaching
=> https://startingstrength.com/event  Events
=> https://aasgaardco.com  Store
=> https://startingstrength.com/careers  Careers
=> https://startingstrength.com/about  About
=> http://www.facebook.com/pages/Starting-Strength-The-Aasgaard-Company/142424022490628  
=> http://twitter.com/SS_strength  
=> https://www.instagram.com/startingstrength  
=> http://www.youtube.com/user/AasgaardCo  
=> https://startingstrength.com/search  
=> https://startingstrength.com/article  Articles
=> https://startingstrength.com/video  Videos
=> https://startingstrength.com/radio  Podcasts
=> https://startingstrength.com/training  Training Log
=> https://startingstrength.com/report  Report
=> https://startingstrength.com/radio  Podcast
=> https://startingstrength.com/resources/forum/  Forums
=> https://startingstrength.com/gyms  Gyms
=> https://startingstrength.com/equipment  Equipment
=> https://startingstrength.com/coaching  Coaching

* Seminars
* Training Camps
* Coach Development
* At SS Gyms
* Find Coaches
* Online Coaching

* Events

* Seminars
* Coaching Camps
* Training Camps
* Meets

* Store
* Careers
* About

* Starting Strength
* Get Started
* Mark Rippetoe
* Who We Are
* The Team
* Contact

* Categories

=> https://startingstrength.com/coaching/seminars  Seminars
=> https://startingstrength.com/coaching/camps  Training Camps
=> https://startingstrength.com/careers  Coach Development
=> https://startingstrength.com/gyms  At SS Gyms
=> https://startingstrength.com/coaching/ssc  Find Coaches
=> https://startingstrengthonlinecoaching.com  Online Coaching
=> https://startingstrength.com/event  Events
=> https://startingstrength.com/event/seminars  Seminars
=> https://startingstrength.com/event/coaching-development  Coaching Camps
=> https://startingstrength.com/event/camps  Training Camps
=> https://startingstrength.com/event/meets  Meets
=> https://aasgaardco.com  Store
=> https://startingstrength.com/careers  Careers
=> https://startingstrength.com/about  About
=> https://startingstrength.com/about  Starting Strength
=> https://startingstrength.com/get-started  Get Started
=> https://startingstrength.com/about/mark-rippetoe  Mark Rippetoe
=> https://startingstrength.com/about/aasgaard  Who We Are
=> https://startingstrength.com/about/team  The Team
=> https://startingstrength.com/about/contact  Contact
=> https://startingstrength.com/article  Categories

* All
* Lifts
* Coaching
* Programming
* Competition
* Military
* Strength & Health
* Science & Medicine
* The Gym
* History
* 中文
* Czech
* Deutsch
* Eesti keel
* Español
* Français
* 日本語
* 한국어
* Bahasa Indonesia
* Italiano
* Português
* русский язык
* Română
* Slovak
* Authors

* All
* Lifts
* Coaching
* Programming
* Competition
* 
* Military
* Strength & Health
* Science & Medicine
* The Gym
* History
* 中文
* Czech
* Deutsch
* Eesti keel
* Español
* Français
* 日本語
* 한국어
* Bahasa Indonesia
* Italiano
* Português
* русский язык
* Română
* Slovak
* Authors

=> https://startingstrength.com/article  All
=> https://startingstrength.com/article/lifts  Lifts
=> https://startingstrength.com/article/coaching  Coaching
=> https://startingstrength.com/article/programming  Programming
=> https://startingstrength.com/article/competition  Competition
=> https://startingstrength.com/article/military  Military
=> https://startingstrength.com/article/strength-health  Strength & Health
=> https://startingstrength.com/article/science-medicine  Science & Medicine
=> https://startingstrength.com/article/gym  The Gym
=> https://startingstrength.com/article/history  History
=> https://startingstrength.com/article/cn  中文
=> https://startingstrength.com/article/cz  Czech
=> https://startingstrength.com/article/de  Deutsch
=> https://startingstrength.com/article/et  Eesti keel
=> https://startingstrength.com/article/es  Español
=> https://startingstrength.com/article/fr  Français
=> https://startingstrength.com/article/jp  日本語
=> https://startingstrength.com/article/kr  한국어
=> https://startingstrength.com/article/in  Bahasa Indonesia
=> https://startingstrength.com/article/it  Italiano
=> https://startingstrength.com/article/pt  Português
=> https://startingstrength.com/article/ru  русский язык
=> https://startingstrength.com/article/ro  Română
=> https://startingstrength.com/article/sk  Slovak
=> https://startingstrength.com/author  Authors
=> https://startingstrength.com/article  All
=> https://startingstrength.com/article/lifts  Lifts
=> https://startingstrength.com/article/coaching  Coaching
=> https://startingstrength.com/article/programming  Programming
=> https://startingstrength.com/article/competition  Competition
=> https://startingstrength.com/article/military  Military
=> https://startingstrength.com/article/strength-health  Strength & Health
=> https://startingstrength.com/article/science-medicine  Science & Medicine
=> https://startingstrength.com/article/gym  The Gym
=> https://startingstrength.com/article/history  History
=> https://startingstrength.com/article/cn  中文
=> https://startingstrength.com/article/cz  Czech
=> https://startingstrength.com/article/de  Deutsch
=> https://startingstrength.com/article/et  Eesti keel
=> https://startingstrength.com/article/es  Español
=> https://startingstrength.com/article/fr  Français
=> https://startingstrength.com/article/jp  日本語
=> https://startingstrength.com/article/kr  한국어
=> https://startingstrength.com/article/in  Bahasa Indonesia
=> https://startingstrength.com/article/it  Italiano
=> https://startingstrength.com/article/pt  Português
=> https://startingstrength.com/article/ru  русский язык
=> https://startingstrength.com/article/ro  Română
=> https://startingstrength.com/article/sk  Slovak
=> https://startingstrength.com/author  Authors

## Articles | programming

### 5 Ways to 5 x 5
by Andy Baker, SSC | June 11, 2014

* [‡ Facebook] >>
* [‡ Twitter] >>
* [‡ Google+] >>
* [‡ Reddit] >>
* [‡ Email] >>
* 

[‡ 5x5 strength training]

=> https://startingstrength.com/author/andy-baker  Andy Baker, SSC
=> https://www.facebook.com/sharer/sharer.php?u=https://startingstrength.com/article%2F5_ways_to_5_x_5&t=5 Ways to 5 x 5 | Andy Baker  >>
=> https://twitter.com/intent/tweet?source=https://startingstrength.com/article%2F5_ways_to_5_x_5&text=Starting%20Strength  >>
=> https://plus.google.com/share?url=https://startingstrength.com/article%2F5_ways_to_5_x_5  >>
=> http://www.reddit.com/submit?url=https://startingstrength.com/article%2F5_ways_to_5_x_5&title=5 Ways to 5 x 5 | Andy Baker  >>
=> ?subject=Starting%20Strength%205 Ways to 5 x 5 | Andy Baker&body=https://startingstrength.com/article%2F5_ways_to_5_x_5  >>
=> https://startingstrength.com/contentfiles/5x5_baker.pdf  

Most of us recognize that doing 5 sets of 5 reps – 5 x 5 – is an incredibly powerful method of getting big and strong. The method is an absolute staple of many of the programs laid out in Practical Programming for Strength Training. The 5-rep set is right in the “metabolic middle,” the perfect combination for both size and strength. And a whole bunch of 5-rep sets is a powerful stimulus. But this article isn’t about physiology. It’s about training. I want you to walk away from this article today with a piece of information you can train with tomorrow.

That being said, there are multiple ways to employ the 5x5 method into your program. Each method has its own set of pros and cons.

So let’s get to it.

=> https://aasgaardco.com/store/books/practical-programming-for-strength-training-328-506  Practical Programming for Strength Training

### 1. Sets Across

This is probably the most commonly used method of doing 5x5, and will be familiar to a large majority of those reading this article. But for those unfamiliar, “sets across” basically means that all 25 reps are done with the same amount of weight. An example session:

* 135 x 5
* 225 x 5
* 315 x 5
* 365 x 5
* 385 x 5
* 405 x 5 x 5 sets (8-10 minutes between sets)

This is by far the hardest method – especially if work sets are approaching that of a 5RM. The benefit of this approach is that it has the potential to create the most stress. The downside is that it has the potential to create the most stress. So going all out on 5x5 across on an exercise like the squat has a tremendous capacity to trigger gains in both size and strength, but it also has the capacity to lead to overtraining. It is also an excellent method for early intermediate lifters to accumulate volume. For a late-stage intermediate, 5x5 across may need to be cycled in with other rep schemes to avoid burnout, or used in conjunction with one of the other 5x5 approaches from this article. For advanced lifters, heavy sets across at this volume will likely be limited to short periods of time (4-8 weeks) before volume must be reduced; otherwise a brush with overtraining can be expected.

No matter the level of training advancement, 5x5 across must be used cautiously by older lifters. If 5 rep sets are to be done heavy, a 40+ lifter might just get the same training effect from 3-4 sets and not run the risk of overtraining. Or he might be better served by using a less stressful variation of the 5x5 protocol.

### 2. Ascending Sets

Ascending sets of 5 involve starting with a light weight set of 5 and progressing up through 5 sets until a limit set of 5 is reached. This method was made most famous by Bill Starr in his classic work The Strongest Shall Survive. There are several key advantages to using this method as opposed to a sets-across approach. First, it’s faster. My guess is that this was the primary reason Starr used this method with his football teams. When going all out with 5x5 across, 8-10 minute rest times between sets are the norm. This often means 40 minutes of rest time alone in a training session, not including warm up sets. For a trainee who wants to get 3-4 exercises in during a training session, this type of drain on his/her time may not be feasible. Using ascending sets, the trainee can usually get through the first three sets on just a couple minutes rest. Prior to his fourth set, he may take 3-5 minutes, and then rest completely (5-10) prior to his limit 5th set. This makes for a much shorter session. For strength coaches or personal trainers who generally have around an hour to work with their clients or athletes, this is can be an ideal approach.

As both pro and con, this method is much less stressful than sets across. For a trainee with limited recuperative abilities (athletes and older trainees) this is a good way to accumulate volume. Additionally these two demographics often require a better warm up than just a general strength trainee. Athletes may be chronically sore and stiff from practice and conditioning, while older folks are generally stiff and sore from being, well, old. The first 2-3 sets at lower weights are a good opportunity for a thorough warm up without wasting a lot of time.

The downside to this method is that it may not generate enough stress to drive adaptation. If this is the case, there are a few “tweaks” you can make. First, simply try adding a set or two. This may mean doing 6 or 7 actual sets instead of just 5. This will “cluster” more work at a higher percentage of your top set. Below is a comparison:

Ascending:

* 135 x 5
* 185 x 5
* 225 x 5
* 275 x 5
* 315 x 5

After tweaking:

* 135 x 5
* 185 x 5
* 225 x 5
* 265 x 5
* 295 x 5
* 315 x 5

A hybrid ascending sets/sets across approach:

* 135 x 5
* 185 x 5
* 225 x 5
* 275 x 5
* 315 x 5 x 2 sets

Another way to make an ascending rep scheme more stressful is to add speed to the light weight sets. So in the rep schemes listed above, make your sets at 135, 185, 225, and 275 move more explosively. Accelerating moderate loads at a high velocity is more stressful to the nervous system than moving that same load slowly. For a sport athlete this is absolutely the recommended approach.

### 3. Descending Sets

Descending sets allow the lifter to work up to his top set of 5 reps faster and with less accumulated fatigue than with ascending sets. Let’s use an example rep scheme of a lifter using 6 ascending sets of 5 to get to a top set of 405x5:

* 135 x 5
* 225 x 5
* 315 x 5
* 365 x 5
* 385 x 5
* 405 x 5

The problem with a setup like this is that the lifter could be handicapping himself on his top set each day. The sets at 365 and 385 are fatiguing enough that they cut into his performance on his top set each day. This will become more pronounced as the lifter grows in strength over time. A simple alternative might be to implement descending sets:

* 135 x 5
* 225 x 3
* 315 x 1
* 365 x 1
* 385 x 1
* 405 x 5
* 395 x 5
* 385 x 5
* 375 x 5
* 365 x 5

This is much much closer to the stress that a sets across approach would create. But because the weight is dropping each set, the rest time between sets could be greatly reduced, making for a much shorter training session than 5x5 across, but still much longer and more stressful than just doing 5 ascending sets. If the lifter attempts to keep rest times short and do each descending set in a state of incomplete recovery this can be an excellent way to improve work capacity and promote hypertrophy.

### 4. Top Set + Back Offs for Sets Across

Many very strong lifters tend to fall under the category of “one and done” performers. For various reasons, these lifters may be very good at producing one solid set of 5 at the target weight, but just don’t have the work capacity, the time, or the mental focus to put together 4 more sets at the same intensity. Descending sets might be a good option for a one and done lifter, but it may not be necessary to reduce the weight on every single back off. Often a simple reduction of about 5% will allow the lifter to get 4 more sets of 5 across. This keeps the aggregate intensity slightly higher than if the lifter were to use descending sets. An example of Back Off Sets Across:

* 135 x 5
* 225 x 3
* 275 x 1
* 315 x 1
* 365 x 1
* 405 x 5
* 385 x 5 x 4 sets

The following week, the lifter would attempt this:

* 410 x 5
* 390 x 5 x 4 sets

### 5. Independent Sets

This is probably the most complex method of employing the 5x5 method. I have just started putting this method into practice within the last year or so. This approach can solve the riddle for the lifter whose 5x5 attempts often look like this:

* 405 x 5
* 405 x 5
* 405 x 4
* 405 x 4
* 405 x 3

In the past, my blanket prescription would have been to reduce the weight on all the sets so that the lifter could have completed all 25 reps in a nice uniform manner. However, after a discussion with one of my consulting clients we tried experimenting with a different approach. To be honest, I wasn’t sure it would work. We said that for every set that was made for 5, we would bump up the following week. For sets that didn’t make 5, we would hold steady. So using the above example, the next week’s attempts would look like this:

* 410
* 410
* 405
* 405
* 405

Amazingly enough, the protocol actually worked pretty well. As the front-end sets went up, the lifter got stronger, which brought up his back end sets the following week. The other major factor was the real and perceived offload when we would go down in weight for the back end sets. I don’t think that factor can be overlooked – it was huge.Here is a hypothetical example of several weeks’ worth of this type of programming:

⊞ table ⊞

Week 1 405 x 5 405 x 5 405 x 4 405 x 4 405 x 3
Week 2 410 x 5 410 x 5 405 x 5 405 x 4 405 x 4
Week 3 415 x 5 415 x 4 410 x 4 405 x 5 405 x 5
Week 4 420 x 5 415 x 5 410 x 5 410 x 4 410 x 4
Week 5 425 x 4 420 x 4 415 x 5 410 x 5 410 x 5
Week 6 425 x 5 420 x 5 420 x 5 415 x 4 415 x 4

The most important point to understand about this method is that each set is progressed independently of the sets around it. So set 2 is only measured against last week’s set 2; set 5 is only measured against last week’s set 5. This is a very, very difficult way to train, but it has worked where it has been applied. Admittedly I do not have a huge database to pull from regarding the effectiveness of the method. I have only applied this to a handful of trainees, and I have only used it with the squat.

Lastly, yes, I recognize that this method is not a true “5x5.” The method tends to produce a high frequency of 4 rep sets. But hey, we use 5s and 3s all the time – what’s wrong with 4s?

There is no uniform prescription on when to utilize one particular method over another. The answer will vary from lifter to lifter, but in general most lifters should start with 5x5 across. This is the simplest method to follow in terms of weight selection each day. There is less to think about and therefore less to screw up. An early intermediate trainee is still fairly inexperienced with the complexities of programming, and is not as in-tune with his body’s performance and recovery capabilities as a more advanced lifter will be. After several weeks or months using a sets-across approach, the lifter will have learned a little more about what he can or cannot tolerate.

If it is decided that there must be some reduction in training stress for continued improvement then ascending sets, descending sets, and back-off sets across are all less stressful variations of the basic 5x5 across method. The independent sets method is a way to force more weight onto the bar. Because the lifter will be dabbling in some heavy sets of 3 and 4 on a regular basis, this is arguably the most stressful method.

In any case, all of these methods can be used as a way to avoid a reset. Many trainees get caught in an endless cycle of resetting weights every time they hit a plateau or sticking point. Sometimes a slight reset in training weights is a necessary and unavoidable action that must be taken to avoid overtraining. But constant resets every time a rep is missed will keep you spinning your wheels in the gym. Both lifters and coaches could stand to be a little more creative in finding new ways to keep adding weight to the bar each and every week. Perhaps one of these 5x5 alternatives can help you.

Discuss in Forums

=> https://startingstrength.com/resources/forum/articles-and-platform-videos/49547-5-5-5-a.html  Discuss in Forums

* [‡ Facebook] >>
* [‡ Twitter] >>
* [‡ Google+] >>
* [‡ Reddit] >>
* [‡ Email] >>
* 

Join the Starting Strength Network
Previous Next
[‡ main starting strength training camps short blue] >>
[‡ main coach dev camps long blue] >>

=> https://www.facebook.com/sharer/sharer.php?u=https://startingstrength.com/article%2F5_ways_to_5_x_5&t=5 Ways to 5 x 5 | Andy Baker  >>
=> https://twitter.com/intent/tweet?source=https://startingstrength.com/article%2F5_ways_to_5_x_5&text=Starting%20Strength  >>
=> https://plus.google.com/share?url=https://startingstrength.com/article%2F5_ways_to_5_x_5  >>
=> http://www.reddit.com/submit?url=https://startingstrength.com/article%2F5_ways_to_5_x_5&title=5 Ways to 5 x 5 | Andy Baker  >>
=> ?subject=Starting%20Strength%205 Ways to 5 x 5 | Andy Baker&body=https://startingstrength.com/article%2F5_ways_to_5_x_5  >>
=> https://startingstrength.com/contentfiles/5x5_baker.pdf  
=> https://network.startingstrength.com  
=> training_optimality  
=> training_performance_for_the_novice_athlete  
=> https://aasgaardco.com/category/training/training-camps/  >>
=> https://aasgaardco.com/category/training/coach-development/  >>

### More from Starting Strength
[‡ back extension in the squat] >>
“Butt Wink” –Mark Rippetoe
[‡ firefighter in gear] >>
Strength Training and the Firefighter –John Musser
[‡ bill march starr] >>
Bill March: The Chosen One, Pt IV –Bill Starr
[‡ novice programming chapter] >>
The Novice Effect –Mark Rippetoe

=> https://startingstrength.com/training/butt-wink  >>
=> https://startingstrength.com/training/butt-wink  
=> https://startingstrength.com/article/strength_training_and_the_firefighter  >>
=> https://startingstrength.com/article/strength_training_and_the_firefighter  
=> https://startingstrength.com/article/bill_march_the_chosen_one_pt_iv  >>
=> https://startingstrength.com/article/bill_march_the_chosen_one_pt_iv  
=> https://startingstrength.com/podcast/the-novice-effect  >>
=> https://startingstrength.com/podcast/the-novice-effect  

### More from Starting Strength
[‡ sprinting] >>

The State of Strength & Conditioning Coaching –Mark Rippetoe

[‡ regaining strength after lyme] >>

Regaining Strength after Lyme Disease –Jayson Ball

=> https://startingstrength.com/article/the-state-of-strength-conditioning-coaching  >>
=> https://startingstrength.com/article/the-state-of-strength-conditioning-coaching  
=> https://startingstrength.com/training/regaining_strength_after_lyme_disease  >>
=> https://startingstrength.com/training/regaining_strength_after_lyme_disease  

### What is Starting Strength?

=> https://startingstrength.com/about  What is Starting Strength?

### Get Started

=> https://startingstrength.com/get-started  Get Started

### Find a Gym

=> https://startingstrength.com/gyms  Find a Gym

### Online Coaching

=> http://startingstrengthonlinecoaching.com  Online Coaching

### Become a Coach

=> https://startingstrength.com/careers  Become a Coach

### Starting Strength Network
Starting Strength Seminars

⊞ table ⊞

Aug 12-14 Wichita Falls, TX
Oct 14-16 Wichita Falls, TX
Dec 9-11 Wichita Falls, TX

Coaching Development Learn to Coach the Lifts

⊞ table ⊞

Oct 1 Columbus, OH

Training Camps Self-Sufficient Lifter

⊞ table ⊞

Sep 10 Wichita Falls, TX

Deadlift & Power Clean

⊞ table ⊞

=> https://network.startingstrength.com  Starting Strength Network
=> https://aasgaardco.com/store/training/starting-strength-seminars/starting-strength-seminar-august-12-14-2022-wichita-falls-tx/  Aug 12-14
=> https://aasgaardco.com/store/training/starting-strength-seminars/starting-strength-seminar-august-12-14-2022-wichita-falls-tx/  Wichita Falls, TX
=> https://aasgaardco.com/store/training/starting-strength-seminars/starting-strength-seminar-october-14-16-2022-wichita-falls-tx/  Oct 14-16
=> https://aasgaardco.com/store/training/starting-strength-seminars/starting-strength-seminar-october-14-16-2022-wichita-falls-tx/  Wichita Falls, TX
=> https://aasgaardco.com/store/training/starting-strength-seminars/starting-strength-seminar-december-9-11-2022-wichita-falls-tx/  Dec 9-11
=> https://aasgaardco.com/store/training/starting-strength-seminars/starting-strength-seminar-december-9-11-2022-wichita-falls-tx/  Wichita Falls, TX
=> https://aasgaardco.com/store/training/coach-development/squat-coaching-development-camp-october-1-columbus-oh/  Oct 1
=> https://aasgaardco.com/store/training/coach-development/squat-coaching-development-camp-october-1-columbus-oh/  Columbus, OH
=> https://aasgaardco.com/store/training/training-camps/the-self-sufficient-lifter-camp-squat-press-deadlift-september-10-2022/  Sep 10
=> https://aasgaardco.com/store/training/training-camps/the-self-sufficient-lifter-camp-squat-press-deadlift-september-10-2022/  Wichita Falls, TX

Aug 20 Indianapolis, IN

Squat & Deadlift

⊞ table ⊞

July 30 London, UK
Aug 21 Bergen, Norway
Aug 27 Beaverton, OR

Squat, Press, & Deadlift

⊞ table ⊞

Aug 27 Greenville, SC
Sep 17 Brussels, Belgium

Meets

⊞ table ⊞

June 25 Omaha, NE
July 9 Omaha, NE
Oct 29 Omaha, NE
Dec 3 Omaha, NE

[‡ starting strength book] >>
[‡ barbell prescription book] >>
[‡ practical programming book] >>
[‡ starting strength online coaching icon] >>
[‡ starting strength seminars] >>
[‡ starting strength app] >>
[‡ starting strength training camps] >>
[‡ lifter's window sticker] >>
[‡ strong enough book] >>
[‡ mean ol mr gravity book] >>
[‡ starting strength shirts] >>
[‡ starting strength mug] >>
[‡ aasgaard mug] >>

Starting Strength Weekly Report

=> https://aasgaardco.com/store/training/training-camps/deadlift-clean-august-20-2022-indianapolis-in/  Aug 20
=> https://aasgaardco.com/store/training/training-camps/deadlift-clean-august-20-2022-indianapolis-in/  Indianapolis, IN
=> https://aasgaardco.com/store/training/training-camps/squat-deadlift-july-30-2022-london-uk/  July 30
=> https://aasgaardco.com/store/training/training-camps/squat-deadlift-july-30-2022-london-uk/  London, UK
=> https://aasgaardco.com/store/training/training-camps/squat-deadlift-august-21-2022-bergen-no/  Aug 21
=> https://aasgaardco.com/store/training/training-camps/squat-deadlift-august-21-2022-bergen-no/  Bergen, Norway
=> https://aasgaardco.com/store/training/training-camps/squat-deadlift-august-27-2022-beaverton-or/  Aug 27
=> https://aasgaardco.com/store/training/training-camps/squat-deadlift-august-27-2022-beaverton-or/  Beaverton, OR
=> https://aasgaardco.com/store/training/training-camps/squat-press-deadlift-august-27-2022-greenville-sc/  Aug 27
=> https://aasgaardco.com/store/training/training-camps/squat-press-deadlift-august-27-2022-greenville-sc/  Greenville, SC
=> https://aasgaardco.com/store/training/training-camps/squat-press-deadlift-september-17-2022-brussels-belgium/  Sep 17
=> https://aasgaardco.com/store/training/training-camps/squat-press-deadlift-september-17-2022-brussels-belgium/  Brussels, Belgium
=> https://www.eventbrite.com/e/2022-testify-strongman-summer-showdown-tickets-236157693017  June 25
=> https://www.eventbrite.com/e/2022-testify-strongman-summer-showdown-tickets-236157693017  Omaha, NE
=> https://www.eventbrite.com/e/2022-testify-ironfest-iv-tickets-235404590467  July 9
=> https://www.eventbrite.com/e/2022-testify-ironfest-iv-tickets-235404590467  Omaha, NE
=> https://www.eventbrite.com/e/2022-testify-fall-classic-tickets-309476280887  Oct 29
=> https://www.eventbrite.com/e/2022-testify-fall-classic-tickets-309476280887  Omaha, NE
=> https://www.eventbrite.com/e/2022-testify-christmas-classic-weightlifting-meet-tickets-324699564137  Dec 3
=> https://www.eventbrite.com/e/2022-testify-christmas-classic-weightlifting-meet-tickets-324699564137  Omaha, NE
=> https://aasgaardco.com/store/books-posters-dvd/books/starting-strength-basic-barbell-training/  >>
=> https://aasgaardco.com/store/books-posters-dvd/books/the-barbell-prescription-strength-training-for-life-after-40/  >>
=> https://aasgaardco.com/store/books-posters-dvd/books/practical-programming-for-strength-training/  >>
=> https://startingstrengthonlinecoaching.com  >>
=> https://aasgaardco.com/category/training/starting-strength-seminars/  >>
=> http://www.shabu.co  >>
=> https://aasgaardco.com/category/training/training-camps/  >>
=> https://aasgaardco.com/store/gear/decals/lifter-sticker/  >>
=> https://aasgaardco.com/store/books-posters-dvd/books/strong-enough/  >>
=> https://aasgaardco.com/store/books-posters-dvd/books/mean-ol-mr-gravity/  >>
=> https://aasgaardco.com/category/gear/shirts/  >>
=> https://aasgaardco.com/store/gear/mugs/starting-strength-mug-2/  >>
=> https://aasgaardco.com/store/gear/mugs/aasgaard-mug/  >>

Highlights from the StartingStrength Community. Browse archives.

Your subscription could not be saved. Please try again.

Your subscription has been successful.

Enter your email address to subscribe

SUBSCRIBE

Get Info

* Articles
* Videos
* Podcasts
* Training
* Search

Get Started

* Lifts
* Programs
* Books
* App
* FAQ

Get Help

* Coaching
* Online Coaching
* Forums
* Events
* Contact Us

© The Aasgaard Company
Privacy Policy | Terms | RSS | Podcast RSS
<style>.geotargetlygeocontent1547782784267_default{display:inline !important}</style>

=> https://startingstrength.com/report/  Browse archives.
=> https://startingstrength.com/article  Articles
=> https://startingstrength.com/video  Videos
=> https://startingstrength.com/podcast  Podcasts
=> https://startingstrength.com/training  Training
=> https://startingstrength.com/search  Search
=> https://startingstrength.com/get-started  Get Started
=> https://startingstrength.com/get-started/lifts  Lifts
=> https://startingstrength.com/get-started/programs  Programs
=> https://startingstrength.com/get-started/equipment  Books
=> https://startingstrength.com/get-started/equipment  App
=> https://startingstrength.com/get-started/faq  FAQ
=> https://startingstrength.com/coaching  Coaching
=> https://startingstrengthonlinecoaching.com  Online Coaching
=> https://startingstrength.com/resources/forum/  Forums
=> https://startingstrength.com/event  Events
=> https://startingstrength.com/about/contact  Contact Us
=> https://startingstrength.com/privacy  Privacy Policy
=> https://startingstrength.com/terms  Terms
=> https://startingstrength.com/rss.rss  RSS
=> http://feeds.soundcloud.com/users/soundcloud:users:38229715/sounds.rss  Podcast RSS

──────────────────── 🦆 ──────────────────── 🦆 ──────────────────── 

Web page filtered and simplified by Duckling Proxy v0.2.1. To view the original content, open the page in your system web browser.
=> https://startingstrength.com/article/programming/5_ways_to_5_x_5 Source page 
