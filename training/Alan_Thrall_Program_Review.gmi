# Starting Strength / Stronglifts
## same program except Stronglifts is 5x5 instead of 3x5 and had rows instead of powerclean
You need to follow the program as close as you can: 3x/week
3x5 squat 3x5 bench/press 1/3x5 deadlift/powerclean
Progress daily: add 5lbs/2.5kg every training day  
## Pros
* teaches lifter how to make progress
* rapidly increases Strength due to linear progression 
* very productive, doing everything you need and nothing you don't
 
## Cons
* Progress stalls out quickly
** need to follow program consistently
** take every training seriously
** eat enough
** sleep enough
* Can get boring due to lack of variation
** You may want to "switch things up"
* SS lacks volume especially presses

# The Texas Method
eat and sleep alot
Like SS, but progress weekly rather than daily
Volume Day, Light Day, Heavy Day
+ 5lbs each week
## Cons
* Long training sessions depending on training; monday sessions can take 2.5 hrs (volume day)\
* Operating at Maximum every week (friday) is tough
* like SS, lacking in volume especially for presses

# 5/3/1
percentage based work
## Pros
* Can make progress for a very long time
* Operating well below max
** Training max = 90% of true max eg actual max = 315lbs -> training max = 285 lbs
* Shooting for rep PRs can keep lifter motivated
** can do more reps on good day, less on bad days
* Manages Fatigue
## Cons
* You need an established 1RM for all of the lifts
* Therefore not suited to beginners, they don't know their 1RM yet
* Lazy can get away with low/minimal 
* No end in sight

# The Juggernaut Method
* lots of volume
Chad Wesley Smith dislikes Mark Rippetoe : "beginner needs lots of volume" vs weight
Sort of an extension of 5/3/1 but lighter weight higher volume 
* Rep Oriented
* Working under your maximum, more encouraging than SS
Duration 4 months
## Cons
* Lots of volume
* You need an established 1RM for all of the lifts

# Super Squats (20 rep Squat Routine)
one of the hardest programs
## Pros
* makes you very Strong
* high volume
* fast progress
* will make every other programm look easy
need to eat a lots
need to fight the fatigue and your mind; you need to really push your mind
try it for 6 weeks at least once in your life
## Cons
* not fun, very hard
* Does not have a huge carryover to your 1RM but improvres your 20 rep squat :-)

# The Cube Method
Heavy day, Rep day, Speed day
Great for recovery because you are never Squatting & Deadlifting heavy in same week
Week 1
Squat: Heavy
Bench: Rep
Deadlift: Speed
Week 2
Squat: Rep
Bench: Speed
Deadlift: Heavy
Week 3
Squat: Speed
Bench: Heavy
Deadlift: Rep

## Pros
* Better for recovery
* Never doing maximum
** easier than SS
* Peaks at end of program, there's an endpoint
## Cons
* no emphasis on overhead press (it emphasis powerlift meets)
* Not enough Bench Stimulus
** increase frequency to compensate
* not designed for beginners, although they progress faster than the program does because their 1RM changes quickly

Olympic Weightlifting Program
12week basic program Greg everett
Catalyst Athletics 
technique focused, less on strength
## Pros
* Plenty of emphasis on the Olympic lifts
* Good amount of variation
* A lot of volume...
## Cons
* Too much volume? more is not always Better
* not enough emphasis on squat, deadlift and overhead press, focus is on clean n jerk & snatch exc
** your other lifts may suffer

Other programs, not covered by Allan Thrall:

#P90x
recommended for
* experienced in fitness
* knowlegable of the basics and beginning phase
* maybe do power90 first?

## Pros
* full body
* no gym needed
## Cons
* need to respect resting and nutrition
* very intense, adrenal fatigue
* injuries if not enough time is taken for recoveries
* may need to take longer than 90d/3w

# Crossfit
## Pros
## Cons

# he Candito 6 week periodized program. Although I'm sure Alan could put it better than I could, here is my take on it that seems to resonate within the lifting community.

1.) Squat: Looking at Candito's lifts, you can see that he's a lower body dominant lifter. His squat and deadlift far outweigh his success in the bench press. That's how I feel about this program, every time I've run the 6 week program my squat has increased the most. I usually don't see a ton of hypertrophy compared to other volume heavy programs, but the strength increase is reliable. The six week program is what I used a few times to get from 330lbs - 440lbs.

2.) Deadlift: I really enjoyed the programming for deadlifts on the 6 week intermediate program. I often find that too much deadlifting destroys my progress in both the squat and deadlift. This is mostly a personal issue due to my proportions (short arms, long femurs; My conventional deadlift is back dominant for sure), however on Candito's program the deadlift programming is low volume, high intensity. I've found I make my best progress this way. Deadlift strength is reliable on this program, although usually less significant than squats.

3.) Bench: This is where I feel Candito's program is seriously lacking and I think he knows it too. The best I ever did on his program was around a 305lb bench at most. There simply isn't enough volume. bench is definitely my strongest lift, so when my bench doesn't increase on a program it tells me the volume just isn't there. I eventually stalled on bench at 305 for around a year until I ran the GZCL method, more specifically, the UHF 5 week program. In 5 weeks I took my bench from 305/310 to around 325. UHF has you doing some sort of bench variation 5 days per week. The volume was insane, but my bench  increased 10-15lbs in 5 weeks after a year long stall. My deadlift stayed the same while my squat increased on the same program.

4.) Who's this program for?: I'd say this program is for anyone who has stalled on linear progression, yet doesn't require a substantial increase in volume in order to make progress. From personal experience, you can run Candito's program without any of the extra assistance work. If you stall that way, add in 2-3 assistance movements to each workout. I stalled on this program after 3-4 cycles, but I never did any extra work, only the required reps and sets. 

I hope this helped anyone who might be looking into Candito's program. Maybe Alan will touch on this a bit in a future video.

  
